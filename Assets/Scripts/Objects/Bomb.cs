﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    new private CircleCollider2D collider;
    private AudioClip clip;
    new private AudioSource audio;


    private void Awake()
    {
        collider = GetComponent<CircleCollider2D>();
        clip = Resources.Load<AudioClip>("Audio/SFX/bomb");
        audio = GetComponent<AudioSource>();
        audio.clip = clip;
    }

    //private void CheckInRadius()
    //{
    //    Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.25F);
    //    int i = 0;
    //    while (i < colliders.Length)
    //    {
    //        //colliders[i].gameObject
    //        i++;
    //    }
    //}
    //private void OnTriggerStay2D(Collider2D collision)
    //{
        
        
    //}
}
