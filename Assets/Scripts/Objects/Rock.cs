﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Rock : MonoBehaviour
{
    new private Collider2D collider;
    private SpriteRenderer render;

    private void Awake()
    {
        collider = GetComponentInChildren<Collider2D>();
        render = GetComponentInChildren<SpriteRenderer>();
        string str = "rocks_basement_" + Random.Range(0, 3);
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/rocks_basement");
        render.sprite = sprites.Single(s => s.name == (str));

    }

    public void Crushed()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/rocks_basement");
        render.sprite = sprites.Single(s => s.name == ("rocks_basement_3"));
    }
}
