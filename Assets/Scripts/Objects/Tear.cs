﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tear : MonoBehaviour
{
    private bool gameIsRunning = true;

    private Vector3 targetDot;

    new private Collider2D collider;
    private SpriteRenderer sprite;
    new private Animator animation;
    private AudioClip clip;
    new private AudioSource audio;
    private Color colorTear = new Color(255, 255, 255);
    private Vector3 basePosition;
    public Color ColorTear
    {
        set { colorTear = value; }
    }

    private bool Flag = false;
    private bool isStop = false;

    private Vector3 direction;
    public Vector3 Direction
    {
        set { direction = value;}
    }

    private float range;
    public float Range
    {
        set { range = value; targetDot = new Vector3(this.transform.position.x + (direction.x * range), this.transform.position.y + (direction.y * range), 2); }
    }

    private float damage;
    public float Damage
    {
        set
        {
            damage = value;
        }
    }

    private float speed;
    public float Speed
    {
        set { speed = value; }
    }

    private Units parent;
    public Units Parent
    {
        set { parent = value; }
    }

    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
        animation = GetComponent<Animator>();
        clip = Resources.Load<AudioClip>("Audio/sfx/tear bloob");
        audio = GetComponent<AudioSource>();
        audio.clip = clip;
        sprite.color = colorTear;
        basePosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (gameIsRunning)
        {
            if (!isStop) transform.position = Vector3.MoveTowards(transform.position, targetDot, speed * Time.deltaTime);
            if(transform.position == targetDot) StartCoroutine(Delay(this.gameObject));

        }
    }

    private void Update()
    {

        if (gameIsRunning)
        {
            if (parent is Character) sprite.transform.localScale = new Vector3(damage * 1.2F, damage * 1.2F);
            else sprite.transform.localScale = new Vector3(2, 2, 2);
            if (parent is Monsters && transform.position == direction) StartCoroutine(Delay(this.gameObject));
        }
    }

    public IEnumerator Delay(GameObject tearObj)
    {
        collider.enabled = false;
        if(tearObj != null) animation.SetBool("Flag", true);
        isStop = true;
        audio.Play();
        yield return new WaitForSeconds(0.4F);
        if(tearObj != null) Destroy(tearObj);
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        Units unit = collider2D.GetComponent<Units>();
        Rock rock = collider2D.GetComponent<Rock>();

        if (unit && unit != parent)
        {
            unit.ResivedDamage(damage);
            animation.SetBool("Flag", true);
            StartCoroutine(Delay(this.gameObject));
        }
        else if (!unit) StartCoroutine(Delay(this.gameObject));
        if (!collider2D.isTrigger && collider.GetComponent<Rock>()) StartCoroutine(Delay(this.gameObject)); ;
    }

    /// Полёт слезы.
    //private IEnumerator FlyTear(Tear tear)
    //{
    //    Debug.Log(range);
    //    // Задержка до уничтожения слезы по окончанию времени.
    //    yield return new WaitForSeconds(range);
    //    if (tear != null) StartCoroutine(tear.Delay(tear.gameObject));
    //}
}
