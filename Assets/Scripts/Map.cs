﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    // Опиание констант.

    /// Поле хранящее максимальный размер карты.
    private const int maxMapSize = 12;

    /// Поле хранящее минимальный размер карты.
    private const int minMapSize = 0;

    /// Поле хранящее центр карты.
    private const int midMapSize = (maxMapSize - minMapSize) / 2;

    /// Поле хранящее максимальное кол-во комнат на этаже.
    private int maxCountRoom;

    /// Поле хранящее ннешнее кол-во комнат на этаже.
    private int countRoom = 1;

    /// Поле хранящее максимальное кол-во видов комнат.
    private int maxNumRoom = 4 + 1;

    /// Поле хранящее направление движения гинератора карты.
    private int direction;

    /// Поле хранящее последнее направление генератора карты.
    private int lustDirection = 0;

    // Флаги, отвечающие за существования типовых комнат.

    /// Флаг отвечающий за команту с предметом.
    private bool treasherRoomExist = false;

    /// Флаг отвечающий за команту с боссом.
    private bool bossRoomExist = false;

    /// \brief Двумерный массив комнат.
    /// Данный массив предназначен для хранения ссылок на комнаты, которые находятся в игровом пространстве.
    /// Это позволяет обрабатывать их и взаимодействовать с каждой комантой по необходимости.
    /// Стартовой точкой является середина карты ([6,6]).
    private Room[,] map;

    /// \brief Каретка используемая для генерации карты.
    /// Эта Каретка хранит свое место расположение, которое используется при генерации карты.
    private Marker marker;

    /// \brief Поле которое хранит расположение персонажа на карте.
    /// Эта Каретка хранит свое место расположение, которое используется при генерации карты.
    private Marker charaMarker;

    
    /// \brief Это метод, который срабатывает после вызова конструктора.
    /// Метод Awake срабатывает в первый кадр сущестоввания объекта.
    /// \warning Данный метод вызывается у объекта только один раз.
    private void Awake()
    {
        StartGeneration();
    }

    /// \brief Это метод, который проверяет на то, что коретка не вышла за границы карты.
    /// \param [in] mark В этот параметр передается каретка.
    private bool BorderCheck(Marker mark)
    {
        return minMapSize < mark.x && mark.x < maxMapSize && minMapSize < mark.y && mark.y < maxMapSize;
    }


    /// \brief Это метод, который передвигает каретку персонажа по карте.
    /// \param [in] direction В этот параметр передается направление движения персонажа.
    public void CharacterMove(string direction)
    {
        if (direction == "Up")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.y--;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Down")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.y++;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Left")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.x++;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
        else if (direction == "Right")
        {
            map[charaMarker.x, charaMarker.y].CharacterInput = false;
            charaMarker.x--;
            map[charaMarker.x, charaMarker.y].CharacterInput = true;
        }
    }

    /// Метод, который используется для создания нового этажа, с удалением предыдущего.
    public void ReGeneration()
    {

        for (int x = 0; x < maxMapSize; x++)
        {
            for (int y = 0; y < maxMapSize; y++)
            {
                if (map[x, y] != null) { Destroy(map[x, y].gameObject); map[x, y] = null; }
            }
        }


        StartGeneration();
    }

    /// \brief Это метод, который подготавливает к генерации все поля и вызывает саму генерацию.
    private void StartGeneration()
    {
        map = new Room[maxMapSize, maxMapSize];

        countRoom = 1;
        maxNumRoom = 4 + 1;
        lustDirection = 0;
        treasherRoomExist = false;
        bossRoomExist = false;
       

        marker.y = midMapSize;
        marker.x = midMapSize;
        maxCountRoom = Random.Range(10, 16);
        countRoom = 1;

        //GameObject newMap = new GameObject();
        //newMap.name = "Map";
        //newMap.transform.parent = transform;
        // Создает первую комнату на этаже.
        Room room = Resources.Load<Room>("Prefabs/Rooms/Room_First");
        map[marker.x, marker.y] = Instantiate<Room>(room, new Vector3(0, 0, 2), Quaternion.identity);
        map[marker.x, marker.y].transform.parent = transform.Find("/WorldSpace").transform;
        map[marker.x, marker.y].x = midMapSize - marker.x;
        map[marker.x, marker.y].y = midMapSize - marker.y;
        map[marker.x, marker.y].CharacterInput = true;
        charaMarker.x = midMapSize;
        charaMarker.y = midMapSize;
        
        do CreateMap(); while (countRoom <= maxCountRoom);

        Marker endMarker = marker;
        ReturnMark();
        while (!CreateTreasherRoom(treasherRoomExist, marker));
        while (!CreateBossRoom(bossRoomExist, endMarker));

        SetDoors();
    }

    /// \brief Это метод, который создает босс-комнату.
    private bool CreateBossRoom(bool flag, Marker mark)
    {
        do direction = Random.Range(1, 5); while (direction == lustDirection);
        lustDirection = direction;
        int Count = Random.Range(2, 5);
        switch (direction)
        {
            case 1: mark.x++; break;
            case 2: mark.y++; break;
            case 3: mark.x--; break;
            case 4: mark.y--; break;
        }

        if (BorderCheck(mark))
        {
            if (map[mark.x, mark.y] == null)
            {
                Room room = Resources.Load<Room>("Prefabs/Rooms/Room_Boss");
                Room cloneRoom = Instantiate<Room>(room, new Vector3(0, 0, 0), Quaternion.identity);
                cloneRoom.transform.parent = transform.Find("/WorldSpace").transform;
                cloneRoom.transform.position = new Vector3((midMapSize - mark.x) * 33, (midMapSize - mark.y) * 20, 2);
                map[mark.x, mark.y] = cloneRoom;

                return flag = true;
            }
        }
        return flag = false;
    }

    /// \brief Это метод, который позволяет создавать команату с предметом.
    private bool CreateTreasherRoom(bool flag, Marker mark)
    {
        do direction = Random.Range(1, 5); while (direction == lustDirection);
        lustDirection = direction;
        int Count = Random.Range(2, 5);
        switch (direction)
        {
            case 1: mark.x++; break;
            case 2: mark.y++; break;
            case 3: mark.x--; break;
            case 4: mark.y--; break;
        }

        if (map[mark.x, mark.y] == null)
        {

            Room room = Resources.Load<Room>("Prefabs/Rooms/Room_Treasher");
            Room cloneRoom = Instantiate<Room>(room, new Vector3(0, 0, 0), Quaternion.identity);
            cloneRoom.transform.parent = transform.Find("/WorldSpace").transform;
            cloneRoom.transform.position = new Vector3((midMapSize - mark.x) * 33, (midMapSize - mark.y) * 20, 2);
            map[mark.x, mark.y] = cloneRoom;

            flag = true;
            return flag;
        }
        flag = false;
        return flag;
    }

    /// \brief Это метод, который позволяет генерировать этаж.
    /// В этом методе описана генерация уровня, путем передвижения каретки marker, по карте (двумерному массиву map[,]). Она двигается в случайном направлении по пустым ячейкам и
    /// загружает в них случайные комнаты, которые загружаются из ресурсов игры.
    private void CreateMap()
    {
        do direction = Random.Range(1, 5); while (direction == lustDirection);
        lustDirection = direction;
        int Count = Random.Range(2, 5);
        for (int i = 0; i < Count; i++)
        {
            if (countRoom <= maxCountRoom)
            {
                switch (direction)
                {
                    case 1: marker.x++; break;
                    case 2: marker.y++; break;
                    case 3: marker.x--; break;
                    case 4: marker.y--; break;
                }

                if (BorderCheck(marker))
                {
                    if (map[marker.x, marker.y] == null)
                    {
                        
                        countRoom++;
                        Room room = Resources.Load<Room>("Prefabs/Rooms/Room_" + Random.Range(1, maxNumRoom));
                        //Debug.Log("Здрасте !" + countRoom + "  " + room.name);
                        Room cloneRoom = Instantiate<Room>(room, new Vector3(0, 0, 0), Quaternion.identity);
                        //Debug.Log("Здрасте !" + countRoom + "  " + cloneRoom.name);
                        cloneRoom.transform.parent = transform.Find("/WorldSpace").transform;
                        cloneRoom.transform.position = new Vector3((midMapSize - marker.x) * 33, (midMapSize - marker.y) * 20, 2);
                        map[marker.x, marker.y] = cloneRoom;
                    }
                }
                else ReturnMark();
            }
        }
    }


    /// Это метод, который задает двери, длвя всех комнат, путем пробега по двумерному массиву.
    private void SetDoors()
    {
        for (int x = 0; x < maxMapSize - 1; x++)
        {
            for (int y = 0; y < maxMapSize - 1; y++)
            {
                if (map[x, y] != null)
                {
                    int i = 0;
                    if (map[x + 1, y] != null) { map[x, y].doorLeftExist = true; i++; }
                    if (map[x - 1, y] != null) { map[x, y].doorRightExist = true; i++; }
                    if (map[x, y + 1] != null) { map[x, y].doorDownExist = true; i++; }
                    if (map[x, y - 1] != null) { map[x, y].doorUpExist = true; i++; }
                }
            }
        }
    }

    //private void SetRoom(Marker cell)
    //{
    //    if (BorderCheck(cell))
    //    {
    //        if (map[cell.x, cell.y] == null)
    //        {
    //            countRoom++;
    //            Room room = Resources.Load<Room>("Prefabs/Rooms/Room_" + Random.Range(1, maxNumRoom));
    //            Room cloneRoom = Instantiate<Room>(room, new Vector3(0, 0, 0), Quaternion.identity);
    //            cloneRoom.transform.parent = transform.Find("/WorldSpace/Map").transform;
    //            cloneRoom.transform.position = new Vector3((midMapSize - marker.x) * 33, (midMapSize - marker.y) * 20, 2);
    //            map[marker.x, marker.y] = cloneRoom;
    //        }
    //    }
    //    else ReturnMark();
    //}

    /// Это метод, который возвращает каретку генератора в середину карты.
    private void ReturnMark()
    {
        marker.y = midMapSize;
        marker.x = midMapSize;
    }

    /// Структура для зранения координат (X и Y).
    /// Transform не используется с целью уменьшения затрат памяти.
    public struct Marker
    {
        public int x;
        public int y;

        public void marker(int p)
        {
            x = p;
            y = p;
        }
    }
}