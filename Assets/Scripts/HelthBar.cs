﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class HelthBar : MonoBehaviour
{
    /// Поле (массив), в котором хранится изображение, которое разбито на сегменты.
    private Sprite[] resources;

    /// Поле (массив), в котором хранятся графические элементы (сердца).
    private GameObject[] helthBar = new GameObject[10];

    /// Поле хранящее, ссылку на объект.
    private Character chara;

    private void Awake()
    {
        chara = transform.Find("/WorldSpace/Character").GetComponent<Character>();
        for (int i = 0; i < 10; i++) helthBar[i] = transform.Find("/WorldSpace/Camera/HB/Heart (" + i + ")").gameObject;
        resources = Resources.LoadAll<Sprite>("Sprites/UI/ui_hearts");
    }

    /// В этом методе описано вырисовывание здоровья.
    /// Добавление/удаление сердец.
    private void Update()
    {
        for (int i = 1; i <= chara.MaxHelth; i++)
        {
            int heartNum = i - 1;
            helthBar[heartNum].gameObject.SetActive(true);
            SpriteRenderer sprite = helthBar[heartNum].transform.Find("Sprite").GetComponent<SpriteRenderer>();
            sprite.sprite = resources.Single(s => s.name == "ui_hearts_2");
        }

        for (int c = 1; c <= (int)chara.Helth; c++)
        {
            int heartNum = c - 1;
            SpriteRenderer sprite = helthBar[heartNum].transform.Find("Sprite").GetComponent<SpriteRenderer>();
            sprite.sprite = resources.Single(s => s.name == "ui_hearts_0");
        }

        float ret = chara.Helth - (int)chara.Helth;

        if(ret == 0.5F)
        {
            SpriteRenderer sprite = helthBar[(int)(chara.Helth + 0.5) - 1].transform.Find("Sprite").GetComponent<SpriteRenderer>();
            sprite.sprite = resources.Single(s => s.name == "ui_hearts_1");
        }
    } 
}
