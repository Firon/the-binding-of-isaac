﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItems : MonoBehaviour
{
    protected Rigidbody2D objectPhysic;
    protected SpriteRenderer render;
    new protected Collider2D collider;

    protected virtual void Awake()
    {
        objectPhysic = transform.GetComponent<Rigidbody2D>();
        render = transform.GetComponentInChildren<SpriteRenderer>();
        collider = transform.GetComponent<Collider2D>();
    }

    protected virtual void Update()
    {

    }
}
