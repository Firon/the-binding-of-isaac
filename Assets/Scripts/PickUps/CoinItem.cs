﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinItem : PickUpItems
{
    private Character chara;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (chara == null)
        {
            chara = collider.GetComponent<Character>();
            if (chara != null)
            {
                chara.Coins += 1;
                Destroy(this.gameObject);
            }
        }
    }
}
