﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : PickUpItems
{
    private Character chara;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (chara == null)
        {
            chara = collider.GetComponent<Character>();
            
            if (chara != null && chara.GetComponent<BoxCollider2D>() == collider.GetComponent<BoxCollider2D>())
            {
                chara.Keys += 1;
                Destroy(this.gameObject);
            }
        }
    }
}
