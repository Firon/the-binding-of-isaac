﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Character : Units
{
    /// \brief Ссылка на игровую камеру.
    /// Используется для передачи места положения персонажа.
    private GameCamera gameCamera;

    ///Перечеслимый тип для состояний аниматора
    private enum HeadAnimationStates
    {
        Idly, ///< Бездействие
        Up, ///< Смотрит вверх
        Horizontal, ///< Смотрит влево/вправо
        Shoot_Up, ///< Стреляет вверх
        Shoot_Horizontal, ///< Стреляет влево/вправо
        Shoot_Down, ///< Стреляет вниз
    }
    
    ///Перечеслимый тип для состояний аниматора.
    private enum BodyAnimationStates
    {
        Idly, ///< Бездействие
        Horizontal, ///< Двигается влево/вправо
        Vertical ///< Двигается вверх/вниз
    }
    

    // Параметры (характеристики) персонажа.

    /// Поле для хранения максимального здоровья персонажа.
    private int maxHelth;

    /// Свойство для хранения максимального здоровья персонажа.
    public int MaxHelth
    {
        set
        {
            maxHelth = value;
            if(value > 0) helth = value;
        }
        get { return maxHelth; }
    }

    /// Свойство для хранения здоровья персонажа.
    public override float Helth
    {
        get { return helth; }

        set
        {
            if (value <= maxHelth) helth = value;
            else helth = maxHelth;
        }
    }

    /// Поле для хранения удачи персонажа.
    /// \warning Данное поле, пока не участвует в игровом процессе (TODO).
    private int luck;


    /// Свойство для хранения удачи персонажа.
    /// \warning Данное свойство, пока не участвует в игровом процессе (TODO).
    public int Luck
    {
        set { luck = value; }
        get { return luck; }
    }

    /// Поле для хранения дальности стрельбы персонажа.
    private float range;
    /// Свойство для хранения дальности стрельбы персонажа.
    public float Range
    {
        set { range = value; }
        get { return range; }
    }

    private float tearSpeed;
    public float TearSpeed
    {
        get { return tearSpeed; }
        set { tearSpeed = value; }
    }

    private bool lokiHornFlag = false;
    public bool LokiHornFlag
    {
        get { return lokiHornFlag; }
        set { lokiHornFlag = value; }
    }

    private int bombs;
    public int Bombs
    {
        get { return bombs; }
        set { bombs = value; }
    }

    private int keys;
    public int Keys
    {
        get { return keys; }
        set { keys = value; }
    }

    private int coins;
    public int Coins
    {
        get { return coins; }
        set { coins = value; }
    }

    /// Компоненты у объекта Character.

    /// Поле хранящее ссылку на компонент, который позволяет обрабатывать физику объекта (столкновения, воздействия сил, импульсов и прочих векторов).
    private Rigidbody2D bodyPhysic;

    /// \brief Поле хранящее ссылку на компонент, описывающий скелета персонажа (контур, границы).
    /// EdgeCollide2D строится по набору точек, заданных разработчиком.
    private EdgeCollider2D headBodyCollider;

    /// \brief Поле хранящее ссылку на компонент, описывающий скелета персонажа (контур, границы).
    private BoxCollider2D bodyCollider;

    /// \brief Ссылка на компонент, хранящая спрайт (изображение) головы персонажа. 
    private SpriteRenderer headSprite;

    /// \brief Ссылка на компонент, хранящая спрайт (изображение) тела персонажа.
    private SpriteRenderer bodySprite;

    /// \brief Ссылка на аниматор, который позволяет обрабатывать анимацию персонажа.
    private Animator animations;

    /// \brief Ссылка на компонент для воспроизведения и настройки звука.
    new private AudioSource audio;

    /// \brief Ссылка на звук/музыку.
    private AudioClip clip;

    /// Флаг отвечающий за то, находится ли сейчас игра в паузе.
    public bool gameIsRunning = true;

    /// Конечный автомат для анимации (Голова).
    HeadAnimationStates headState
    {
        get { return (HeadAnimationStates)animations.GetInteger("Head_State"); }

        set { animations.SetInteger("Head_State", (int)value); }
    }

    /// Конечный автомат для анимации (Тело).
    BodyAnimationStates bodyState
    {
        get { return (BodyAnimationStates)animations.GetInteger("Body_State"); }

        set { animations.SetInteger("Body_State", (int)value); }
    }

    // Флаги.
    private bool isReloaded = true;
    private bool IsntDamaged = true;

    protected bool waitFlagCam;
    public bool WaitFlagCam
    {
        set { waitFlagCam = value; }
        get { return waitFlagCam; }
    }

    /// \brief Это метод, который срабатывает после вызова конструктора.
    /// Метод Awake срабатывает в первый кадр сущестоввания объекта.
    /// \warning Данный метод вызывается у объекта только один раз.
    private void Awake()
    {
        // Описываем компонент Audio.
        audio = gameObject.AddComponent<AudioSource>();
        audio.volume = 0.2F;
        audio.priority = 100;
        audio.pitch = 1;
        audio.panStereo = 0;
        audio.reverbZoneMix = 1;

        // Связываем коммпоненты с существующими.
        bodyPhysic = GetComponentInChildren<Rigidbody2D>();
        headBodyCollider = GetComponent<EdgeCollider2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
        headSprite = transform.Find("/WorldSpace/Character/Head").gameObject.GetComponent<SpriteRenderer>();
        bodySprite = transform.Find("/WorldSpace/Character/Body").gameObject.GetComponent<SpriteRenderer>();
        animations = GetComponent<Animator>();


        gameCamera = transform.Find("/WorldSpace/Camera").GetComponent<GameCamera>();

        
        waitFlagCam = true;

        // Задаем базовые характеристики.
        speed = 4;
        rate = 5;
        damage = 1.5F;
        maxHelth = 3;
        helth = maxHelth;
        range = 4 * 1.5F;
        tearSpeed = 10;
    }

    /// \brief Метод который срабатывает фиксированное кол-во раз (60 раз секнду).
    /// Данный метод, лучше использовать при физических вычислениях и перемещениях объекта.
    private void FixedUpdate()
    {
        gameIsRunning = SceneManager.sceneCount == 1;
        if (gameIsRunning)
        {
            if ((Input.GetButton("Horizontal") || Input.GetButton("Vertical")) && waitFlagCam) Move();
            else if((Input.GetButtonUp("Horizontal") || Input.GetButtonUp("Vertical")))
            {
                headState = HeadAnimationStates.Idly;
                bodyState = BodyAnimationStates.Idly;
            }
        }
    }

    /// \brief Метод срабатывает при каждом обновлении кадра (при переходе с кадра на кадр).
    /// Данный метод, лучше использовать для логики объекта.
    private void Update()
    {
        // Запущена ли игра ?
        if (gameIsRunning)
        {
            // При нажатии на Escape включается меню.
            if (Input.GetKeyUp(KeyCode.Escape)) SceneManager.LoadScene("EscMenu", LoadSceneMode.Additive);

            // Обрабатывается нажатие на стрелки клавиатуры, для стрельбы.
            if ((Input.GetButton("FireHorizontal") || Input.GetButton("FireVertical")) && isReloaded) Shoot();
            // Если персонаж не двигается, то передаем состояние бездействия в анимацию.
            else if ((Input.GetButtonUp("FireHorizontal") || Input.GetButtonUp("FireVertical")))
            {
                // Для анимации тела.
                bodyState = BodyAnimationStates.Idly;
                
                // Для анимации головы.
                headState = HeadAnimationStates.Idly;
            }
        }
    }

    /// Описание метода, реализующего движения персонажа.
    private void Move()
    {
        // Перемнная для получения дагонали.
        float diagonal = 1;

        // Переменная для получения направления движения, путем получения направления вектора (+1 или -1).
        Vector3 direction = new Vector3(0, 0, 0);

        // Получение направления по x.
        direction.x += Input.GetAxis("Horizontal");
        
        // Получение направления по y.
        direction.y += Input.GetAxis("Vertical");

        // Оперделяем, движется ли персонаж по лиагонали, если да, то мы должны уменьшить его скорость передвижения,
        // так как она увеличится вдвое.
        if (Input.GetButton("Horizontal") && Input.GetButton("Vertical")) diagonal = 0.85F;

        // При нажатии вверх или вниз (W,S).
        if (Input.GetButton("Vertical"))
        {
            // Передаем состаяние аниматору.
            bodyState = BodyAnimationStates.Vertical;

            // Передаем состаяние аниматору, при условии того, что песронаж не стреляет.
            if (Input.GetAxis("Vertical") > 0 && !Input.GetButton("FireVertical") && !Input.GetButton("FireHorizontal")) headState = HeadAnimationStates.Up;
            else if (Input.GetAxis("Vertical") < 0 && !Input.GetButton("FireVertical") && !Input.GetButton("FireHorizontal")) headState = HeadAnimationStates.Idly;
        }
        // При нажатии влево или вправо (A,D).
        else if (Input.GetButton("Horizontal"))
        {
            bodyState = BodyAnimationStates.Horizontal;
            if (!Input.GetButton("FireVertical") && !Input.GetButton("FireHorizontal")) headState = HeadAnimationStates.Horizontal;

            // Инверсия изображения через scale.

            // Если персонаж бежит влево, то голова поворачивается (инверсия изображения) 
            if (Input.GetAxis("Horizontal") < 0 && headSprite.transform.localScale.x > 0 && !Input.GetButton("FireVertical") && !Input.GetButton("FireHorizontal")) headSprite.transform.localScale = new Vector3(-1, 1);  
            else if (Input.GetAxis("Horizontal") > 0 && headSprite.transform.localScale.x < 0 && !Input.GetButton("FireVertical") && !Input.GetButton("FireHorizontal"))
            {
                headSprite.transform.localScale = new Vector3(1, 1);
            }
            
            // Такая же инверсия, только для тела.
            if (Input.GetAxis("Horizontal") < 0 && bodySprite.transform.localScale.x > 0)
            {
                bodySprite.transform.localScale = new Vector3(-1, 1);
            }
            else if (Input.GetAxis("Horizontal") > 0 && bodySprite.transform.localScale.x < 0)
            {
                bodySprite.transform.localScale = new Vector3(1, 1);
            }
        }

        // Двигаем персонажа от начального положения (transform.position) к заданой точке(transform.position + direction), с указаной скоростью (((speed * 2.5F) * diagonal) * Time.deltaTime)),
        // где Time.deltaTime, используется для обновления кадров в движении.  
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, ((speed * 2.5F) * diagonal) * Time.deltaTime);
    }

    /// Метод реализующий стрельбу персонажа.
    private void Shoot()
    {
        // Дляя направления 
        Vector3 direction = new Vector3(0, 0, 0);

        // Получаем направление.
        direction.x += Input.GetAxis("FireHorizontal");
        direction.y += Input.GetAxis("FireVertical");

        // При нажатии на стрелки (Up, Down).
        if (Input.GetButton("FireVertical"))
        {
            // Передача состаяний при стрельбе (только для головы персонажа).
            if (Input.GetAxis("FireVertical") > 0) headState = HeadAnimationStates.Shoot_Up;
            else if (Input.GetAxis("FireVertical") < 0) headState = HeadAnimationStates.Shoot_Down;
        }
        // При нажатии на стрелки (Left, Right).
        else if (Input.GetButton("FireHorizontal"))
        {
            // Передача состаяний при стрельбе (только для головы персонажа).
            headState = HeadAnimationStates.Shoot_Horizontal;
            if (Input.GetAxis("FireHorizontal") < 0 && headSprite.transform.localScale.x > 0)
            {
                headSprite.transform.localScale = new Vector3(-1, 1);
            }
            else if (Input.GetAxis("FireHorizontal") > 0 && headSprite.transform.localScale.x < 0)
            {
                headSprite.transform.localScale = new Vector3(1, 1);
            }
        }


        // Создаем снаряд для стрельбы (слеза).
        // Загружаем заготовленную заранее слезу (префаб), который хранится в ресурсах игры и создаем ссылку на неё.
        Tear tear = Resources.Load<Tear>("Prefabs/Tear");
        
        // Создаем игровой объект (слеза), используя функцию Instantiate и передаем ей параметры: оригинал, по которому создается объект-клон, позицию создаваемого объекта, 
        // вращение объекта.
        Tear cloneTear = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);
        
        // Передаем параметры.

        // Кто родитель.
        cloneTear.Parent = this;
        // Передаем скорость.
        cloneTear.Speed = tearSpeed;
        // Направление.
        cloneTear.Direction = direction;

        // Урон.
        cloneTear.Damage = damage;

        cloneTear.Range = range;

        if (LokiHornFlag) modifLokiShoot(direction);

        // Запускаем перезарядку.
        StartCoroutine(reloading());
    }

    private void modifLokiShoot(Vector3 direct)
    {

        Tear tear = Resources.Load<Tear>("Prefabs/Tear");
        Tear cloneTear1 = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);

        direct.y *= -1;
        cloneTear1.Parent = this;
        cloneTear1.Speed = tearSpeed;
        cloneTear1.Direction = direct;
        cloneTear1.Damage = damage;

        cloneTear1.Range = range;


        Tear cloneTear2 = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);

        direct.x *= -1;

        cloneTear2.Parent = this;
        cloneTear2.Speed = tearSpeed;
        cloneTear2.Direction = direct;
        cloneTear2.Damage = damage;

        cloneTear2.Range = range;


        Tear cloneTear3 = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);

        direct.x *= -1;
        direct.y *= -1;
        cloneTear3.Parent = this;
        cloneTear3.Speed = tearSpeed;
        cloneTear3.Direction = direct;
        cloneTear3.Damage = damage;

        cloneTear3.Range = range;
    }

    /// Метод проверки на соприкосновение с объектом.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Берем из списка объектов, с которыми сопрекоснулся персонаж, объект с типом Pedistal.
        Pedistal pedistal = collision.GetComponent<Pedistal>();
        if(pedistal != null)
        {
            // Если такой есть, то получаем артефакт, с этого пъедистала.
            pedistal.TakeItem();
        }
    }

    /// Переопредленный метод, отвечающий за получение урона.
    public override void ResivedDamage(float damage)
    {
        // Если он может получить урон (временной интервал с получения последнего удара окончен), то получаем урон.
        if (IsntDamaged)
        {
            base.ResivedDamage(damage);
            StartCoroutine(GetDamage());
        }
    }

    /// Метод описывающий смерть персонажа.
    protected override void Die()
    {
        // Загружаем музыку из ресурсов.
        clip = Resources.Load<AudioClip>("Audio/sfx/Character/isaac dies new");
        // Загружаем её в компонент и запускаем.
        audio.clip = clip; audio.Play();
        // Запуск экрана смерти.
        StartCoroutine(Dieing());
        gameCamera.audioSystem.mute = true;
        //this.gameObject.SetActive(false);
    }


    /// Все корутины используемые персонажем.

    /// Используется при смерти, с задержкой, вызывает экран смерти.
    private IEnumerator Dieing()
    {
        // TODO добавить анимацию смерти.
        yield return new WaitForSeconds(0.5F);
        if(gameIsRunning) SceneManager.LoadScene("WillMenu", LoadSceneMode.Additive);
    }

    /// Перезарядка выстрелов.
    private IEnumerator reloading()
    {
        isReloaded = false;
        yield return new WaitForSeconds(rate * 0.08F);
        isReloaded = true;
    }

    /// Получение урона
    private IEnumerator GetDamage()
    {
        /// Загружаем звук из ресурсов.
        clip = Resources.Load<AudioClip>("Audio/sfx/Character/hurt grunt  " + Random.Range(0, 2));

        /// Загружаем его в компонент.
        audio.clip = clip;
        /// Запускаме.
        audio.Play();

        /// Включаем задержку на запрет получения урона.
        IsntDamaged = false;

        /// Добавляем красный оттенок спрайтам.
        headSprite.color = Color.red;
        bodySprite.color = Color.red;
        yield return new WaitForSeconds(0.6F);
        /// Снимаем оттенок.
        headSprite.color = new Color(255,255,255);
        bodySprite.color = new Color(255, 255, 255);
        /// Разрешамем получение урона.
        IsntDamaged = true;
    }
}
