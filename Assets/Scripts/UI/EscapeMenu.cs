﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeMenu : MonoBehaviour
{
    private GameObject marker;


    private Vector3 optionsPoint;
    private Vector3 resumGamePoint;
    private Vector3 exitGamePoint;

    private int item = 0;
    private int maxNumItem;

    private void Awake()
    {
        // Определяем точки пунктов меню.
        optionsPoint = new Vector3(-4.8F, -1.6F);
        resumGamePoint = new Vector3(-5.6F, -3.6F);
        exitGamePoint = new Vector3(-4.4F, -5.4F);

        // Создаем ссылку на маркер и переносим его к первому пункту меню.
        marker = transform.Find("/Canvas/Marker").gameObject;
        marker.transform.position = optionsPoint;
        

        // Определяем макс. номер элемента меню.
        maxNumItem = 2;
        item = 0;
    }

    private void Update()
    {
        // Движение стрелки меню.
        if ((Input.GetKeyDown("down") || Input.GetKeyDown("s")) && (item <= maxNumItem))
        {
            if (item == maxNumItem) item = 0;
            else item++;
        }
        else if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && (item >= 0))
        {
            if (item == 0) item = maxNumItem;
            else item--;
        }

        switch (item)
        {
            case 0: marker.transform.position = optionsPoint; break;
            case 1: marker.transform.position = resumGamePoint; break;
            case 2: marker.transform.position = exitGamePoint; break;
        }

        if (Input.GetKeyUp("return"))
        {
            if (item == 0)
            {

            }
            else if (item == 1)
            {
                SceneManager.UnloadSceneAsync("EscMenu");
            }
            else if (item == 2)
            {
                SceneManager.LoadScene("Main");
            }
        }
    }
}
