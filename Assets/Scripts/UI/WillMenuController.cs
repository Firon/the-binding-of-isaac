﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class WillMenuController : MonoBehaviour
{
    private GameObject menu;
    new private AudioSource audio;
    private AudioClip clip;

    private void Awake()
    {
        audio = gameObject.AddComponent<AudioSource>();
        clip = Resources.Load<AudioClip>("Audio/Music/you died");

        audio.volume = 0.25F;
        audio.playOnAwake = true;
        audio.priority = 128;
        audio.pitch = 1;
        audio.reverbZoneMix = 1;
        audio.panStereo = 0;
        audio.clip = clip;
        audio.Play();
    }

    private void Update()
    {
        if (Input.GetKey("return") || Input.GetKey("space"))
        {
            SceneManager.LoadScene("Game", LoadSceneMode.Single);
        }
        else if (Input.GetKey("escape"))
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
        if (!audio.isPlaying) audio.Play();
    }
}
