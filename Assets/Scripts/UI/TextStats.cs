﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextStats : MonoBehaviour
{
    private Text stats;
    private Character character;

    private void Awake()
    {
        stats = GetComponent<Text>();
        character = transform.Find("/WorldSpace/Character").GetComponent<Character>();
        //transform.position = new Vector3(transform.position.x, transform.position.y, 2);
    }

    private void Update()
    {
        stats.text = "*" + character.Coins + "\n*" + character.Keys  +  "\n*" + character.Bombs;
    }
}
