﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class GUIController : MonoBehaviour
{
    // Графические компоненты.
    private Image logo;
    private Image backGround_1;
    private Image pressStartLogo;
    private Image fly;
    private Image white_Effect;

    // Пункты меню.
    private GameObject marker;
    private GameObject newRunButton;
    private GameObject optionsButton;
    private GameObject exitButton;
    private int item = 0;
    private int maxNumItem = 2;

    // Камера.
    new private MenuCamera camera;

    // Триггреы.
    private bool flagPSLogo = true;
    private bool flagLogo = true;
    private bool onMenu = false;
    
    // Переменные, для передвижения объектов.
    private float direct = 0.002F;
    private float camSpeed = 7;

    // Массив изображений.
    private Sprite[] resources;
    
    // Тип для опредления пункта в главном меню.
    private enum Section
    {
        intro,
        menu
    }
    private Section section = Section.intro;

    // Точки.
    // Точки меню для камеры.
    private Vector3 introPoint = new Vector3(0, 0, -10);
    private Vector3 menuPoint = new Vector3(0, -10, -10);

    // Точки для маркера, переходы между пунктами меню.
    private Vector3 newGamePoint;
    private Vector3 optionsButtonPoint;
    private Vector3 exitButtonPoint;

    

    private void Awake()
    {
        camera = transform.Find("/Main/Camera").GetComponent<MenuCamera>();
        camera.Speed = 0.1F;
        // Intro.  
        logo = transform.Find("/Main/Intro/Logo").GetComponent<Image>();
        fly = transform.Find("/Main/Intro/Fly").GetComponent<Image>();
        pressStartLogo = transform.Find("/Main/Intro/Press_Start_Logo").GetComponent<Image>();
        backGround_1 = transform.Find("/Main/Intro/Background_1").GetComponent<Image>();

        // Menu.
        marker = transform.Find("/Main/Menu/Menu_Controller/Marker").gameObject;
        newRunButton = transform.Find("/Main/Menu/Menu_Controller/NewRun_Button").gameObject;
        optionsButton = transform.Find("/Main/Menu/Menu_Controller/Options_Button").gameObject;
        exitButton = transform.Find("/Main/Menu/Menu_Controller/Exit_Button").gameObject;

        resources = Resources.LoadAll<Sprite>("Sprites/Main Menu/titlemenu");
        white_Effect = transform.Find("/Main/Intro/White_Effect").GetComponent<Image>();
        white_Effect.color = new Color(1, 1, 1, 1);

        // Определяем расположение кнопок.
        newGamePoint = newRunButton.transform.position;
        newGamePoint.x = -2.2F;
        newGamePoint.y = -9.2F;

        optionsButtonPoint = optionsButton.transform.position;
        optionsButtonPoint.x = -1.7F;
        optionsButtonPoint.y = -10.4F;

        exitButtonPoint = exitButton.transform.position;
        exitButtonPoint.x = -1.3F;
        exitButtonPoint.y = -11.7F;

        marker.transform.position = newGamePoint;
    }



    private void FixedUpdate()
    {
        
        if (section == Section.intro)
        {
            //
            if (camera.transform.position != introPoint) camera.Move(introPoint);

            if (Input.anyKey && !(Input.GetKey(KeyCode.Escape)) && (white_Effect.color.a < 0)) section = Section.menu;
            else if (Input.GetKey(KeyCode.Escape)) Application.Quit();
            // Анимация Press Start.
            if (flagPSLogo) StartCoroutine(DelayFramePSLogo(0.1F));

            // Вращение логотипа.
            if (logo.transform.rotation.z > 0.026F) direct = -0.003F;
            else if (logo.transform.rotation.z < -0.026F) direct = 0.003F;
            if (flagLogo) StartCoroutine(DelayLogo(0.1F));
            // 
        }
        else if (section == Section.menu)
        {
            if (camera.transform.position != menuPoint) camera.Move(menuPoint);
            else onMenu = true;
        }  
    }

    private void Update()
    {
        // Затухание экрана при запуске.
        if (white_Effect.color.a > 0) StartCoroutine(NewName());

        // Контроллер для меню.
        if (onMenu)
        {
            // Движение стрелки меню.
            if ((Input.GetKeyDown("down") || Input.GetKeyDown("s")) && (item <= maxNumItem))
            {
                if (item == maxNumItem) item = 0;
                else item++;
            }
            else if ((Input.GetKeyDown("up") || Input.GetKeyDown("w")) && (item >= 0)) 
            {
                if (item == 0) item = maxNumItem;
                else item--;
            }
            //


            if (item == 0) marker.transform.position = newGamePoint;
            else if (item == 1) marker.transform.position = optionsButtonPoint;
            else if (item == 2) marker.transform.position = exitButtonPoint;

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown("space")) switch (item)
            {
                case 0: SceneManager.LoadScene("Game"); break;
                case 1:
                
                
                    break;
                case 2: Application.Quit(); break;
                
            }

        }
    }
    
    private IEnumerator NewName()
    {
        yield return new WaitForSeconds(0.1F);
        white_Effect.color -= new Color(0, 0, 0, 0.01F);
        // Debug.Log(white_Effect.color);
    }

    private IEnumerator DelayFramePSLogo(float time)
    {
        flagPSLogo = false;
        yield return new WaitForSeconds(time);
        if (pressStartLogo.sprite.name == "titlemenu_2") pressStartLogo.sprite = resources.Single(s => s.name == ("titlemenu_3"));
        else pressStartLogo.sprite = resources.Single(s => s.name == ("titlemenu_2"));
        flagPSLogo = true;
    }

    private IEnumerator DelayLogo(float time)
    {
        flagLogo = false;
        yield return new WaitForSeconds(time);
        logo.transform.rotation = new Quaternion(logo.transform.rotation.x, logo.transform.rotation.y, logo.transform.rotation.z + direct, logo.transform.rotation.w);
        flagLogo = true;
    }
}


