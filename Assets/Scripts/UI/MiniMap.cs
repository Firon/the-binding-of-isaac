﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MiniMap : MonoBehaviour
{
    private const int mapMax = 12;
    private const int mapMin = 0;
    private const int mapMid = (mapMax - mapMin) / 2;

    private SpriteRenderer[,] miniMap = new SpriteRenderer[mapMax, mapMax];

    private Sprite[] resources;

    private Marker marker;

    private void Awake()
    {
        resources = Resources.LoadAll<Sprite>("Sprites/UI/minimap1");
        marker.x = mapMid;
        marker.y = mapMid;

        GameObject objectClone = new GameObject();
        objectClone.transform.SetParent(transform, false);

        miniMap[mapMid, mapMid] = objectClone.AddComponent<SpriteRenderer>();
        miniMap[mapMid, mapMid].sprite = resources.Single(s => s.name == "minimap1_31");
    }

    public void Up()
    {
        miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_5");
        resources = Resources.LoadAll<Sprite>("Sprites/UI/minimap1");
        marker.y--;
        if (miniMap[marker.x, marker.y] == null)
        {
            GameObject objectClone = new GameObject();
            objectClone.transform.SetParent(transform, false);
            objectClone.transform.position = new Vector3(objectClone.transform.position.x + (mapMid - marker.x) * 0.68F, objectClone.transform.position.y + (mapMid - marker.y) * 0.6F, 2);

            miniMap[marker.x, marker.y] = objectClone.AddComponent<SpriteRenderer>();
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
        else
        {
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
    }

    public void Down()
    {
        miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_5");
        resources = Resources.LoadAll<Sprite>("Sprites/UI/minimap1");
        marker.y++;
        if (miniMap[marker.x, marker.y] == null)
        {
            GameObject objectClone = new GameObject();
            objectClone.transform.SetParent(transform, false);
            objectClone.transform.position = new Vector3(objectClone.transform.position.x + (mapMid - marker.x) * 0.68F, objectClone.transform.position.y + (mapMid - marker.y) * 0.6F, 2);

            miniMap[marker.x, marker.y] = objectClone.AddComponent<SpriteRenderer>();
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
        else
        {
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
    }

    public void Left()
    {
        miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_5");
        resources = Resources.LoadAll<Sprite>("Sprites/UI/minimap1");
        marker.x++;
        if (miniMap[marker.x, marker.y] == null)
        {
            GameObject objectClone = new GameObject();
            objectClone.transform.SetParent(transform, false);
            objectClone.transform.position = new Vector3(objectClone.transform.position.x + (mapMid - marker.x) * 0.68F, objectClone.transform.position.y + (mapMid - marker.y) * 0.6F, 2);

            miniMap[marker.x, marker.y] = objectClone.AddComponent<SpriteRenderer>();
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
        else
        {
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
    }

    public void Right()
    {
        miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_5");
        resources = Resources.LoadAll<Sprite>("Sprites/UI/minimap1");
        marker.x--;
        if (miniMap[marker.x, marker.y] == null)
        {
            GameObject objectClone = new GameObject();
            objectClone.transform.SetParent(transform, false);
            objectClone.transform.position = new Vector3(objectClone.transform.position.x + (mapMid - marker.x) * 0.68F, objectClone.transform.position.y + (mapMid - marker.y) * 0.6F, 2);

            miniMap[marker.x, marker.y] = objectClone.AddComponent<SpriteRenderer>();
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
        else
        {
            miniMap[marker.x, marker.y].sprite = resources.Single(s => s.name == "minimap1_31");
        }
    }

    private bool BorderCheck(Marker mark)
    {
        return mapMin < mark.x && mark.x < mapMax && mapMin < mark.y && mark.y < mapMax;
    }

    private void ReturnMark()
    {
        marker.y = mapMid;
        marker.x = mapMid;
    }

    public struct Marker
    {
        public int x;
        public int y;

        public void marker(int p)
        {
            x = p;
            y = p;
        }
    }
}
