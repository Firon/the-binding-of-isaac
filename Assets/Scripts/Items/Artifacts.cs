﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifacts : MonoBehaviour
{
    protected Character character;
    protected SpriteRenderer spriteArtifact;

    // Характеристики персонажа.

    protected int maxHelth;
    protected float helth;
    protected float speed;
    protected int rate;
    protected float damage;
    protected int luck;
    protected float range;
    protected bool lokiHornFlag = false; 

    /// Этот метод сделан виртуальным, для того чтобы можно было переопределить его в дочерних классах.
    /// В дочерних классах, в этом методе указываются поля (характеристики), которые будут задавать песронажу.
    protected virtual void Awake()
    {
        character = transform.Find("/WorldSpace/Character").GetComponent<Character>();
    }

    protected virtual void Update()
    {
        
    }

    // Метод, для передачи параметров персонажу.
    public void SetStats()
    {
        character.Helth += helth;
        character.Speed += speed;
        character.Range += range;
        character.Rate += rate;
        character.Luck += luck;
        character.Damage += damage;
        character.MaxHelth += maxHelth;
        character.LokiHornFlag = lokiHornFlag;
    }
}
