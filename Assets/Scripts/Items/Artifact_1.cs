﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_1 : Artifacts
{

    protected override void Awake()
    {
        base.Awake();
        damage = 1;
        rate = -2;
        range = 1;
    }
}
