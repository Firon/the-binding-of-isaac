﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Artifact_3 : Artifacts
{
    protected override void Awake()
    {
        base.Awake();
        damage = (character.Damage * 1.2F) + 0.5F;
    }
}
