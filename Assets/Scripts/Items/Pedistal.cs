﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedistal : MonoBehaviour
{

    new private Collider2D collider;
    new private Rigidbody2D rigidbody;
    private Artifacts artifact;
    public Artifacts Artifact { get { return artifact; } }
    new public AudioSource audio;

    private Room room;

    /// Поле, которе хранит направление движения артифакта (для анимации).
    private float direct = 0.01F;

    /// Поле, которое хроанит максимальный номер предмета, из ресурсов.
    private int maxArtifactNum = 5+1;

    private void Awake()
    {
        // Создаем компонент AudioSource.
        audio = gameObject.AddComponent<AudioSource>();
        audio.playOnAwake = true;
        audio.volume = 0.25F;
        audio.priority = 150;
        audio.pitch =   1;
        audio.panStereo = 0;
        audio.reverbZoneMix = 1;

        audio.clip = Resources.Load<AudioClip>("Audio/sfx/holy!");

        artifact = ArtifactGenerate();
        room = transform.parent.GetComponent<Room>();
    }

    /// Вызывается при смене кадров.
    private void Update()
    {
        // Если артифакт есть.
        if (artifact != null)
        {
            // То добавляем эфект парения в воздухе. 
            if (artifact.transform.position.y >= transform.position.y + 1.5F) direct *= -1;
            else if (artifact.transform.position.y <= transform.position.y + 0.9F) direct *= -1;
            // Запускаем задержки между кадрами.
            StartCoroutine(FrameDelay());
        }
        if (!room.CharacterInput && artifact == null) Destroy(this.gameObject);
    }

    /// Метод для загрузки случайного артифакта, из ресурсов игры.
    /// \return Возвращает ссылку на новый объект.
    private Artifacts ArtifactGenerate()
    {
        // Загружаем из игры.
        Artifacts artif = Resources.Load<Artifacts>("Prefabs/Items/Artifacts/Artifact_" + Random.Range(1, maxArtifactNum));

        // Создаем объект-клон.
        Artifacts newArtifact = Instantiate(artif, transform.position + new Vector3(0, 1F, 0), Quaternion.identity) as Artifacts;

        // Задаем родителя.
        newArtifact.transform.parent = transform;

        // Возвращаем полученый артифакт.
        return newArtifact;
    }

    /// Корутин для задержки кадра и пермещения артифакта.
    protected IEnumerator FrameDelay()
    {
        yield return new WaitForSeconds(0.25F);
        if(artifact != null) artifact.transform.position += new Vector3(0, direct, 0);
    }

    /// Метод для передачи параметров предмета, запуска звука, уничтожение объекта.
    public void TakeItem()
    {
        if (artifact != null)
        {
            audio.Play();
            artifact.SetStats();
            Destroy(artifact.gameObject);
        }
    }
}
