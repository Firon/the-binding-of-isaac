using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private float speed = 5;
    private Character chara;
    private AudioClip clip;
    public AudioSource audioSystem;

    private void Awake()
    {
        chara = transform.Find("/WorldSpace/Character").GetComponent<Character>();
        clip = Resources.Load<AudioClip>("Audio/Music/diptera sonata intro");
        audioSystem = GetComponent<AudioSource>();
        audioSystem.volume = 0.2F;
        audioSystem.clip = clip;
        audioSystem.Play();
        clip = Resources.Load<AudioClip>("Audio/Music/diptera sonata(basement)");
    }

    private void Update()
    {
        // ��������� ����� ��� �����.
        if (!chara.gameIsRunning)
        {
            audioSystem.volume = 0.15F;
        }
        else
        {
            audioSystem.volume = 0.2F;
        }

        if(!audioSystem.isPlaying)
        {
            audioSystem.clip = clip;
            audioSystem.Play();
        }
    }

    public void CameraMove(float x, float y)
	{
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(x, y, 0), (speed * 10) * Time.deltaTime);
        if (transform.position != new Vector3(x, y, 0)) chara.WaitFlagCam = false;
        else chara.WaitFlagCam = true;
    }
}