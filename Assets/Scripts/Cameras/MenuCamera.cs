﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    private float speed;
    public float Speed { set { speed = value; } }
    new private AudioSource audio;
    private AudioClip clip;

    private void Awake()
    {
        // Создаем компонент AudioSource.
        audio = gameObject.AddComponent<AudioSource>();
        audio.playOnAwake = true;
        audio.volume = 0;
        audio.priority = 128;
        audio.pitch = 1;
        audio.panStereo = 0;
        audio.reverbZoneMix = 1;

        audio.clip = Resources.Load<AudioClip>("Audio/Music/title screen intro");
        audio.Play();
        clip = Resources.Load<AudioClip>("Audio/Music/title screen");
    }
    private void Update()
    {
        if (audio.volume < 0.25F) StartCoroutine(VolumeInc());
        if (!audio.isPlaying)
        {
            audio.clip = clip;
            audio.Play();
        }
    }

    private IEnumerator VolumeInc()
    {
        yield return new WaitForSeconds(0.2F);
        audio.volume += 0.1F;
    }

    public void Move(Vector3 point)
    {
        StartCoroutine(Acceleration(0.3F));
        transform.position = Vector3.MoveTowards(transform.position, point, speed * Time.deltaTime);
    }

    private IEnumerator Acceleration(float incSpeed)
    {
        yield return new WaitForSeconds(0.2F);
        speed += incSpeed;
    }
}
