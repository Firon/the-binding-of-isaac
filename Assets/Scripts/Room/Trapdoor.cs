﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trapdoor : MonoBehaviour
{
    new private Collider2D collider;
    new private GameCamera camera;
    private Character chara;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
        camera = transform.Find("/WorldSpace/Camera").GetComponent<GameCamera>();
        chara = transform.Find("/WorldSpace/Character").GetComponent<Character>();
    }

    // Срабатывает при пересечеии колайдеров (соприкосновении).
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Получаем пользователя.
        Character chara = collision.GetComponent<Character>();

        // Если он сопрекоснулся с дверью.
        if (chara && !collision.isTrigger)
        {
            // Получаем объект, который является игровым пространством.
            Map map = transform.Find("/WorldSpace").GetComponent<Map>();
            camera.transform.position = new Vector3(0, 0, 0);
            chara.transform.position = new Vector3(0, -2.18F, 2);

            // Вызываем генерацию мира.
            map.ReGeneration();
        }
    }
}
