﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Door : MonoBehaviour
{
    private MiniMap miniMap;
    new private Collider2D collider;
    private Map map;
    private Room room;
    public bool isClosed;

    public bool ToTheTreasher;

    // Массив изображений.
    private Sprite[] resources;

    private void Start()
    {
        room = transform.parent.GetComponent<Room>();
        collider = GetComponent<Collider2D>();
        map = transform.Find("/WorldSpace").GetComponent<Map>();

        if (room.name == "Room_Boss(Clone)")
        {
            // Массив изображений.
            Sprite[] resources = Resources.LoadAll<Sprite>("Sprites/Rooms/door_10_bossroomdoor");
            SpriteRenderer doorSprite = transform.Find("/WorldSpace" + room.name + "/" + name + "/DoorSprite").gameObject.GetComponent<SpriteRenderer>();
            SpriteRenderer backgroundSprite = transform.Find("/WorldSpace" + room.name + "/" + name + "/BackgroundSprite").gameObject.GetComponent<SpriteRenderer>();


            backgroundSprite.transform.localScale = new Vector3(1.5F, 1.2F);
            doorSprite.sprite = resources.Single(s => s.name == "door_10_bossroomdoor_0");
            backgroundSprite.sprite = resources.Single(s => s.name == "door_10_bossroomdoor_1");
        }

        miniMap = transform.Find("/WorldSpace/Camera/MiniMap").GetComponent<MiniMap>();
    }
    private void Update()
    {
        // Дверь закрыта, если в команте есть враги.
        isClosed = !room.IsEmpty;
    }

    // Метод обрабатывает, что в дверь проходит персонаж.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Character chara = collision.GetComponent<Character>();

        if (chara)
        {
            // если дверь не закрыта и персонаж внутри комнаты, то передвигаем персонажа.
            if(!isClosed && room.CharacterInput) Transport(chara);
        }
    }

    // Перемещение персонажа по команатам.
    public void Transport(Character chara)
    {
        
        if (name == "Door_Up")
        {
            // Перемещяем персонажа в другую комнату.
            chara.transform.position = new Vector3(transform.position.x, transform.position.y + 6.7F, 2);
            
            // Двигаем маркер на карте.
            map.CharacterMove("Up");
            
            // Двигаем персонажа на миникарте.
            miniMap.Up();
        }
        else if (name == "Door_Down")
        {
            // Перемещяем персонажа в другую комнату.
            chara.transform.position = new Vector3(transform.position.x, transform.position.y - 7, 2);
            
            // Двигаем маркер на карте.
            map.CharacterMove("Down");
            
            // Двигаем персонажа на миникарте.
            miniMap.Down();
        }
        else if (name == "Door_Left")
        {
            // Перемещяем персонажа в другую комнату.
            chara.transform.position = new Vector3(transform.position.x - 11, transform.position.y, 2);
            
            // Двигаем маркер на карте.
            map.CharacterMove("Left");
            
            // Двигаем персонажа на миникарте.
            miniMap.Left();
        }
        else if (name == "Door_Right")
        {
            // Перемещяем персонажа в другую комнату.
            chara.transform.position = new Vector3(transform.position.x + 11, transform.position.y, 2);
            
            // Двигаем маркер на карте.
            map.CharacterMove("Right");

            // Двигаем персонажа на миникарте.
            miniMap.Right();
        }
    }
}
