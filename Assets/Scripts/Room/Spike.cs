﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour {
	
	void Update ()
    {
	    	
	}

    private void OnTriggerStay2D(Collider2D collider)
    {
        Character character = collider.GetComponent<Character>();
        if(character != null && !collider.isTrigger)
        {
            character.ResivedDamage(0.5F);
        }
    }
}
