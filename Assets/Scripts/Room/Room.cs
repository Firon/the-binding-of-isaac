﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    private GameCamera cameraController;
    private Character chara;

    public int x;
    public int y;

    public bool IsBoss
    {
        get { return name == "Room_Boss"; }
    }

    private bool lootDropped = false;

    public bool IsEmpty
    {
        get { return transform.Find("Monsters").gameObject.transform.childCount == 0; }
    }

    private bool characterInput = false;
    public bool CharacterInput
    {
        set { characterInput = value; }
        get { return characterInput; }
    }

    public bool doorUpExist
    {
        set { transform.Find("Door_Up").gameObject.SetActive(value); }
    }
    public bool doorDownExist
    {
        set { transform.Find("Door_Down").gameObject.SetActive(value); }
    }
    public bool doorLeftExist
    {
        set { transform.Find("Door_Left").gameObject.SetActive(value); }
    }
    public bool doorRightExist
    {
        set { transform.Find("Door_Right").gameObject.SetActive(value); }
    }

    private void Awake()
    {
        cameraController = transform.Find("/WorldSpace/Camera").GetComponent<GameCamera>();
        chara = transform.Find("/WorldSpace/Character").GetComponent<Character>();
    }

    private void Update()
    {
        if (characterInput && (cameraController.transform.position != transform.position))
        {
            cameraController.CameraMove(transform.position.x, transform.position.y);
        }
    }
}
