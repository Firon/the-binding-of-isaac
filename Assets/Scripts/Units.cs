﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units : MonoBehaviour
{
    /// Поле хранящее здоровье.
    protected float helth;

    public virtual float Helth
    {
        set { helth = value; }
        get { return helth; }
    }

    /// Поле хранящее скорость передвижения.
    protected float speed;
    public float Speed
    {
        set { speed = value; }
        get { return speed; }
    }

    /// Поле хранящее скорость стрельбы.
    protected int rate;
    public int Rate
    {
        set { rate = value; }
        get { return rate; }
    }

    /// Поле хранящее урон.
    protected float damage;
    public float Damage
    {
        set { damage = value; }
        get { return damage; }
    }

    /// Метод в котором описано получение урона.
    public virtual void ResivedDamage(float damage)
    {
        helth -= damage;

        if (helth <= 0) Die();
    }


    /// Метод в котором описана смерть существа.
    protected virtual void Die()
    {
        Destroy(this.gameObject);
    }
}
