﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_2 : Monsters
{
    private bool isReloaded = false;
    new private Animator animation;

    private enum AnimationState
    {
        sleep,
        wakeUp,
        atack
    }
    private AnimationState state
    {
        set { animation.SetInteger("State",(int)value); }

        get { return (AnimationState)animation.GetInteger("State"); }
    }

    protected override void Awake()
    {
        base.Awake();
        speed = Random.Range(2, 6);
        helth = 7.5F;
        audio.volume = 0.2F;
        animation = GetComponent<Animator>();
    }

    private void Update()
    {
        if (target.gameIsRunning )
        {
            Vector3 heading = target.transform.position - transform.position;

            if (state == AnimationState.sleep && room.CharacterInput && target.WaitFlagCam && heading.sqrMagnitude < (80)) StartCoroutine(WakeUp());
            else if (state != AnimationState.sleep && room.CharacterInput && target.WaitFlagCam && heading.sqrMagnitude < (80))
            {
                if (isReloaded && room.CharacterInput) Atack(0.5F);
                if (!room.CharacterInput) audio.volume = 0;
            }
            else
            {
                state = AnimationState.sleep;
                isReloaded = false;
            }
        }
    }

    private IEnumerator WakeUp()
    {
        state = AnimationState.wakeUp;
        if (!isReloaded) yield return 0;
        isReloaded = false;
        yield return new WaitForSeconds(0.6F);
        isReloaded = true;
    }

    protected override void Atack(float damage)
    {
        Tear tear = Resources.Load<Tear>("Prefabs/Tear");
        Tear cloneTear = Instantiate<Tear>(tear, transform.position + new Vector3(0, 0.2F), Quaternion.identity);
        cloneTear.Parent = this;
        cloneTear.Speed = 5;
        
        if (target != null) cloneTear.Direction = target.transform.position - transform.position;
        cloneTear.Damage = damage;
        cloneTear.ColorTear = Color.red;
        StartCoroutine(reloading());
        state = AnimationState.atack;
    }

    private IEnumerator reloading()
    {
        isReloaded = false;
        yield return new WaitForSeconds(1);
        isReloaded = true;
    }

    protected override void OnTriggerStay2D(Collider2D collision)
    {
        Character chara = collision.GetComponent<Character>();

        if (chara == target)
        {
            target.ResivedDamage(0.5F);
        }
    }
}
