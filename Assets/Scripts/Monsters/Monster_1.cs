﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_1 : Monsters
{
    protected override void Awake()
    {
        base.Awake();
        speed = Random.Range(2, 6);
        helth = 3.5F;
        clip = Resources.Load<AudioClip>("Audio/sfx/insect swarm");
        audio.clip = clip;
        audio.volume = 0.2F;
    }

    private void Update()
    {
        if (target.gameIsRunning)
        {
            if (!audio.isPlaying && room.CharacterInput) audio.Play();
            Move();
        }
    }
}
