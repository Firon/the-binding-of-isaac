﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monsters : Units
{
    protected Character target;
    new protected Collider2D collider;
    protected Room room;
    new protected AudioSource audio;
    protected AudioClip clip;

    /// 
    protected virtual void Awake()
    {
        collider = GetComponent<Collider2D>();
        target = transform.Find("/WorldSpace/Character").gameObject.GetComponent<Character>();
        room = transform.parent.parent.GetComponent<Room>();
        audio = GetComponent<AudioSource>();

    }

    protected virtual void OnTriggerStay2D(Collider2D collision)
    {
        Character charact = collision.GetComponent<Character>();
        if (charact == target)
        {
            Atack(0.5F);
        }
    }

    /// Метод в котором описана атака монстра.
    /// \param [in] damage В этот параметр передается урон монстра.
    protected virtual void Atack(float damage)
    {
        if(target != null)target.ResivedDamage(0.5F);
    }


    /// Метод в котором описано передвижение монстра.
    protected virtual void Move()
    {
        if (target != null && room.CharacterInput && target.WaitFlagCam) transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
    }
}
